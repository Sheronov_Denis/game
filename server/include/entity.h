#ifndef ENTITY_H
#define ENTITY_H

#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <vector>

#include "game.pb.h"
#include "level.h"

#define CH_T_PISTOL   50
#define N_PISTOL      1
#define DIS_PISTOL    0
#define CH_T_SHOTGUN  200
#define N_SHOTGUN     5
#define DIS_SHOTGUN  -2
#define ZOMB_BEGIN_SPEED 0.5

class Entity {
  public:
    float x;
    float y;
    float rotation;
    float width;
    float height;
    bool  life;
    std::vector<Object> map;

    Entity() {}
    Entity(float x_, float y_, float w, float h) : x(x_), y(y_), width(w), height(h) {
      life = true;
    }
    virtual ~Entity() = default;
    sf::FloatRect getRect() {
      return sf::FloatRect(x, y, width, height);
    }

    void set_collisions(Level &lvl) {
      map = lvl.GetObjects("solid");
    }

    bool check_collision_with_map(float dx, float dy) {
      bool is_bump = false;

      for (int i = 0; i < map.size(); i++) {
        if (getRect().intersects(map[i].rect)) {  // is_bump не надо в каждом ифе
          if (dy > 0) {y -= dy;}
          if (dy < 0) {y -= dy;}
          if (dx > 0) {x -= dx;}
          if (dx < 0) {x -= dx;}
          is_bump = true;
        }
      }

      return is_bump;
    }
};

class Bullet : public Entity {
  public:
    int   id;
    float dx;
    float dy;
    float life_time;

    Bullet(float x, float y, float w, float h) : Entity(x, y, w, h) {life_time = 0;}
    void set_direction(float mouse_x, float mouse_y);
    void update(float time);
};

class BulletsVector {
  public:
    std::vector<std::unique_ptr<Bullet>> bullets;
    int id;

    BulletsVector() {id = 0;}
    void new_bullet(std::unique_ptr<Bullet> bullet) {
      bullets.push_back(std::move(bullet));
    }
    void update(float time);
    void write_bullets_in_mes(pack::FromServ* message);
};

class Client : public Entity {
  public:
    class Weapon {
      public:
        bool  ready_fire;
        float charge_time;
        float current_time;
        int   n_bullets;
        int   dispersion;
        Level lvl;

        Weapon() {}
        Weapon(Level &lvl_, float ch_t, float n, int disp) {
          ready_fire = true;
          charge_time = ch_t;
          n_bullets = n;
          current_time = 0;
          lvl = lvl_;
          dispersion = disp;
        }
        void calc_bullets(Client* client, BulletsVector* bullets, float mouse_x, float mouse_y, float time);
    };
    int   id;
    int   points;
    bool  attack;
    bool  ready_fire;
    float charge_time;
    Weapon current_weapon;
    Weapon pistol;
    Weapon shotgun;
    Level lvl;

    Client() : Entity() {}
    Client(float x, float y, float w, float h) : Entity(x, y, w, h) {
      attack = false;
      ready_fire = true;
      charge_time = 0;
      points = 0;
    }
    void set_lvl(Level &lvl_) {
      lvl = lvl_;
      set_collisions(lvl);
      Weapon pistol_(lvl, CH_T_PISTOL, N_PISTOL, DIS_PISTOL);
      pistol = pistol_;
      current_weapon = pistol_;
      Weapon shotgun_(lvl, CH_T_SHOTGUN, N_SHOTGUN, DIS_SHOTGUN);
      shotgun = shotgun_;
    }
    void calc_move(int moves, float time, float mouse_x, float mouse_y, BulletsVector* bullets);
    void calc_rot(float mouse_x, float mouse_y);
};

class Zombie : public Entity {
  public:
    int  id;
    bool attack;
    float speed;
    bool is_bump;
    float bump_time;

    Zombie(float x, float y, float w, float h) : Entity(x, y, w, h) {
      attack = false;
      is_bump = false;
      bump_time = 0;
    }
    void set_speed(float speed_) {speed = speed_;}
    void set_id(int id_) {id = id_;}
    void calc_move(Client& client, float time);
    void calc_rot(Client& client);
    void update(Client& client, float time);
};

class ZombiesVector {
  public:
    std::vector<std::unique_ptr<Zombie>> zombies;
    float all_zombies_speed;
    int id;

    ZombiesVector() {all_zombies_speed = ZOMB_BEGIN_SPEED; id = 0;}
    void create_zombies(Level& lvl);
    void update(float time, Client* client_obj_1, Client* client_obj_2);
    void write_zombies_in_mes(pack::FromServ* message);
};

void client_recive_and_send(Client& client_obj, sf::TcpSocket* client, float time, BulletsVector* bullets);
sf::Socket::Status send_all(Client& client_obj_1, Client& client_obj_2, sf::TcpSocket* client_1, pack::FromServ message);
void set_client(pack::FromServ* message, Client& client_obj_1);
void check_collision_zombie_x_bullet(ZombiesVector* zombies, BulletsVector* bullets, Client* client_1, Client* client_2);
void check_collision_zombie_x_client(float time, ZombiesVector* zombies, Client* client);

#endif  // ENTITY_H