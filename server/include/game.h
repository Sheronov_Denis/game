#ifndef GAME_H
#define GAME_H

#include <SFML/Network.hpp>
#include <iostream>
#include <vector>

#include "game.pb.h"
#include "entity.h"
#include "level.h"

class Game {
  public:
    sf::TcpSocket* client_1;
    sf::TcpSocket* client_2;
    ZombiesVector zombies_vector;
    BulletsVector bullets;
    Level lvl;
    Object player_1;
    Object player_2;
    Client client_obj_1;
    Client client_obj_2;
    sf::Packet packet;
    bool is_disconnect;

    Game(sf::TcpSocket* client_1_, sf::TcpSocket* client_2_) {
      is_disconnect = false;
      client_1 = client_1_;
      client_2 = client_2_;
      lvl.LoadFromFile("map.tmx");

      zombies_vector.create_zombies(lvl);

      player_1 = lvl.GetObject("player1");
      Client client_obj_1_(player_1.rect.left, player_1.rect.top, player_1.rect.width, player_1.rect.height);
      client_obj_1 = client_obj_1_;
      client_obj_1.set_lvl(lvl);
      client_obj_1.id = 0;

      player_2 = lvl.GetObject("player2");
      Client client_obj_2_(player_2.rect.left, player_2.rect.top, player_2.rect.width, player_2.rect.height);
      client_obj_2 = client_obj_2_;
      client_obj_2.set_lvl(lvl);
      client_obj_2.id = 1;

      packet << true;
      client_1->send(packet);
      client_2->send(packet);
      packet.clear();
      packet << client_obj_1.id << client_obj_2.id;
      client_1->send(packet);
      packet.clear();
      packet << client_obj_2.id << client_obj_1.id;
      client_2->send(packet);
      packet.clear();
    }

    void update(float time) {
      pack::FromServ message;

      time = time / 10;
      client_recive_and_send(client_obj_1, client_1, time, &bullets);
      client_recive_and_send(client_obj_2, client_2, time, &bullets);
      bullets.update(time);
      zombies_vector.update(time, &client_obj_1, &client_obj_2);
      check_collision_zombie_x_bullet(&zombies_vector, &bullets, &client_obj_1, &client_obj_2);
      check_collision_zombie_x_client(time, &zombies_vector, &client_obj_1);
      check_collision_zombie_x_client(time, &zombies_vector, &client_obj_2);
      zombies_vector.write_zombies_in_mes(&message);
      bullets.write_bullets_in_mes(&message);
      sf::Socket::Status cl_1_stat = send_all(client_obj_1, client_obj_2, client_1, message);
      sf::Socket::Status cl_2_stat = send_all(client_obj_2, client_obj_1, client_2, message);
      client_obj_1.attack = false;
      client_obj_2.attack = false;
      if ((cl_1_stat == sf::Socket::Status::Disconnected) && (cl_2_stat == sf::Socket::Status::Disconnected)) {
        is_disconnect = true;
      }
    }
};

#endif //  GAME_H
