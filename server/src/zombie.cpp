#include <math.h>

#include "game.pb.h"
#include "entity.h"

#define ZOMBIES_SPEED_BOOST 1.2
#define BUMP_TIME           25

void Zombie::calc_move(Client& client, float time) {
  float dx = (client.x - x);
  float dy = (client.y - y);
  float normal = sqrt(dx * dx + dy * dy);
  dx *= 1/normal;
  dy *= 1/normal;
  if (is_bump) {
    x -= dx * time * speed / 2;
    y -= dy * time * speed;
    check_collision_with_map(-dx * time * speed / 2, -dy * time * speed);
    bump_time += time;
    if (bump_time > BUMP_TIME) {
      is_bump = false;
      bump_time = 0;
    }
  } else {
    x += dx * time * speed;
    y += dy * time * speed;
    is_bump = check_collision_with_map(dx * time * speed, dy * time * speed);
  }
}

void Zombie::calc_rot(Client& client) {
  float dX = client.x - x;
  float dY = client.y - y;
  rotation = (atan2(dY, dX)) * 180 / M_PI;
}

void Zombie::update(Client& client, float time) {
  calc_move(client, time);
  calc_rot(client);
}

void ZombiesVector::create_zombies(Level& lvl) {
  std::vector<Object> zombie_map = lvl.GetObjects("zombie");
  for (int i = 0; i < zombie_map.size(); i++) {
    auto zombie = std::make_unique<Zombie>(zombie_map[i].rect.left,
                                           zombie_map[i].rect.top,
                                           zombie_map[i].rect.width,
                                           zombie_map[i].rect.height);
    zombie->set_speed(all_zombies_speed);
    zombie->set_id(id);
    id++;
    zombie->set_collisions(lvl);
    zombies.push_back(std::move(zombie));
  }
}

void ZombiesVector::write_zombies_in_mes(pack::FromServ* message) {
  for (int i = 0; i < zombies.size(); i++) {
    pack::ServObjInfo* cell = message->add_serv_obj_info();
    cell->set_type(pack::ServObjInfo::ZOMBIE);
    cell->set_life(zombies[i]->life);
    cell->set_attack(zombies[i]->attack);
    cell->set_id(zombies[i]->id);
    cell->set_x(zombies[i]->x);
    cell->set_y(zombies[i]->y);
    cell->set_rotation(zombies[i]->rotation);
    zombies[i]->attack = false;
    if (!(zombies[i]->life)) {
      zombies.erase(zombies.begin() + i);
    }
  }
}

void ZombiesVector::update(float time, Client* client_obj_1, Client* client_obj_2) {
  if ((client_obj_1->life) || (client_obj_2->life)) {
    for (int i = 0; i < zombies.size(); i++) {
      if (zombies[i]->id % 2) {
        if (client_obj_1->life) {
          zombies[i]->update(*client_obj_1, time);
        } else {
          zombies[i]->update(*client_obj_2, time);
        }
      } else {
        if (client_obj_2->life) {
          zombies[i]->update(*client_obj_2, time);
        } else {
          zombies[i]->update(*client_obj_1, time);
        }
      }
    }
    if (zombies.empty()) {
      create_zombies(client_obj_1->lvl);
      all_zombies_speed *= ZOMBIES_SPEED_BOOST;
    }
    for (int i = 0; i < zombies.size(); i++) {
      for (int j = 0; j < zombies.size(); j++) {
        if (i != j) {
          if (zombies[i]->getRect().intersects(zombies[j]->getRect())) {
            float dx = 0;
            float dy = 0;
            if (zombies[i]->id % 2) {
              if (client_obj_1->life) {
                dx = (client_obj_1->x - zombies[i]->x);
                dy = (client_obj_1->y - zombies[i]->y);
              } else {
                dx = (client_obj_2->x - zombies[i]->x);
                dy = (client_obj_2->y - zombies[i]->y);
              }
            } else {
              if (client_obj_2->life) {
                dx = (client_obj_2->x - zombies[i]->x);
                dy = (client_obj_2->y - zombies[i]->y);
              } else {
                dx = (client_obj_1->x - zombies[i]->x);
                dy = (client_obj_1->y - zombies[i]->y);
              }
            }
            float speed = sqrt(dx * dx + dy * dy);
            dx *= 1/speed;
            dy *= 1/speed;
            zombies[i]->x -= dx * (time ) * all_zombies_speed;
            zombies[i]->y -= dy * (time ) * all_zombies_speed;
            zombies[i]->check_collision_with_map(-(dx * time) * all_zombies_speed, -(dy * time) * all_zombies_speed);
            zombies[j]->x += dx * (time ) * all_zombies_speed;
            zombies[j]->y += dy * (time ) * all_zombies_speed;
            zombies[j]->check_collision_with_map((dx * time) * all_zombies_speed, (dy * time) * all_zombies_speed);
          }
        }
      }
    }
  }
}
