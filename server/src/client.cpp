#include <math.h>
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <iostream>

#include "game.pb.h"
#include "entity.h"

void Client::calc_move(int moves, float time, float mouse_x, float mouse_y, BulletsVector* bullets) {
  switch (moves & 15) {
    case (1):
      x += time;
      check_collision_with_map(time, 0.0);  // S
      break;
    case (2):
      x -= time;
      check_collision_with_map(-time, 0.0);  // W
      break;
    case (4):
      y += time;
      check_collision_with_map(0.0, time);  // A
      break;
    case (8):
      y -= time;
      check_collision_with_map(0.0, -time); // D
      break;
    case (5):
      x += time * 0.707;
      y += time * 0.707;
      check_collision_with_map(time * 0.707, time * 0.707); // AS
      break;
    case (6):
      x -= time * 0.707;
      y += time * 0.707;
      check_collision_with_map(-time * 0.707, time * 0.707); // AW
      break;
    case (9):
      x += time * 0.707;
      y -= time * 0.707;
      check_collision_with_map(time * 0.707, -time * 0.707); // DS
      break;
    case (10):
      x -= time * 0.707;
      y -= time * 0.707;
      check_collision_with_map(-time * 0.707, -time * 0.707); // WD
      break;
  }
  if ((moves & (1 << 4)) != 0) {
    current_weapon.calc_bullets(this, bullets, mouse_x - width/2, mouse_y, time);
  }
  if ((moves & (1 << 4)) == 0) {
    current_weapon.current_time += time;
  } 
  if ((moves & (1 << 5)) != 0) {
    if (current_weapon.n_bullets != pistol.n_bullets) {
      shotgun = current_weapon;
      current_weapon = pistol;
    }
  }
  if ((moves & (1 << 6)) != 0) {
    if (current_weapon.n_bullets != shotgun.n_bullets) {
      pistol = current_weapon;
      current_weapon = shotgun;
    }
  }
  calc_rot(mouse_x, mouse_y);
}

void Client::calc_rot(float mouse_x, float mouse_y) {
  float dX = mouse_x - x - width/2;
  float dY = mouse_y - y - height/2;
  rotation = (atan2(dY, dX)) * 180 / M_PI;
}

void client_recive_and_send(Client& client_obj, sf::TcpSocket* client, float time, BulletsVector* bullets) {
  sf::Packet packet;
  std::string cli;
  pack::ClientObjInfo info;

  if (client->receive(packet) == sf::Socket::Done) {
    packet >> cli;
    info.ParseFromString(cli);
    if (client_obj.life) {
      client_obj.calc_move(info.bitmask(), 1.5 * time, info.mouse_x(), info.mouse_y(), bullets);
    }
  }
}

sf::Socket::Status send_all(Client& client_obj_1, Client& client_obj_2, sf::TcpSocket* client_1, pack::FromServ message) {
  sf::Packet packet;

  set_client(&message, client_obj_1);
  set_client(&message, client_obj_2);

  std::string for_client_1 = "";
  message.SerializeToString(&for_client_1);
  packet << for_client_1;
  sf::Socket::Status status;
  while ((status = client_1->send(packet)) == sf::Socket::Partial) {}

  return status;
}

void set_client(pack::FromServ* message, Client& client_obj) {
  pack::ServObjInfo* cell = message->add_serv_obj_info();
  cell->set_type(pack::ServObjInfo::CLIENT);
  if (client_obj.current_weapon.n_bullets == client_obj.pistol.n_bullets) {
    cell->set_w_type(pack::ServObjInfo::M16);
  } else {
    cell->set_w_type(pack::ServObjInfo::SHOTGUN);
  }
  cell->set_id(client_obj.id);
  cell->set_life(client_obj.life);
  cell->set_points(client_obj.points);
  cell->set_x(client_obj.x);
  cell->set_y(client_obj.y);
  cell->set_rotation(client_obj.rotation);
  cell->set_attack(client_obj.attack);
}

void check_collision_zombie_x_client(float time, ZombiesVector* zombies_vec, Client* client) {
  for (int i = 0; i < zombies_vec->zombies.size(); i++) {
    if (zombies_vec->zombies[i]->getRect().intersects(client->getRect())) {
      zombies_vec->zombies[i]->attack = true;
      client->life = false;
    }
  }
}
