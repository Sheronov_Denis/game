#include <SFML/Network.hpp>
#include <iostream>

#include "game.h"

int main(int argc, char* argv[]) {
  if (argc != 2) {
    std::cout << "Usage: ./server.out port" << std::endl;
    return 1;
  }
  std::vector<std::unique_ptr<Game>> sessions;
  std::vector<sf::TcpSocket*> sockets;

  sf::TcpListener listener;
  if (listener.listen(atoi(argv[1])) != sf::Socket::Done) {
    std::cout << "There is no possibility to listen on this port" << std::endl;
    return 1;
  }

  sf::SocketSelector selector;
  selector.add(listener);
  
  sf::Clock clock;

  float time = 0;
  clock.restart();

  int client_count = 0;
  int socket_count = 0;

  while (true) {
    time = clock.getElapsedTime().asMilliseconds();
    clock.restart();
    selector.wait(sf::microseconds(10.f));
    if (selector.isReady(listener)) {
      sf::TcpSocket* client = new sf::TcpSocket;
      listener.accept(*client);
      sockets.push_back(client);
      client_count++;
      if (client_count == 2) {
        auto session = std::make_unique<Game>(sockets[sockets.size() - 1], sockets[sockets.size() - 2]);
        sessions.push_back(std::move(session));
        client_count = 0;
      }
    }
    for (int i = 0; i < sessions.size(); i++) {
      sessions[i]->update(time);
      if (sessions[i]->is_disconnect == true) {
        delete sessions[i]->client_1;
        delete sessions[i]->client_2;
        sessions.erase(sessions.begin() + i);
      }
    }

    sf::Time cycle_time = clock.getElapsedTime();
    sf::sleep(sf::milliseconds(1000/30) - cycle_time);
  }

  return 0;
}
