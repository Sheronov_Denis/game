#include <math.h>
#include <iostream>

#include "game.pb.h"
#include "entity.h"

#define BULLET_LIFE_TIME 200

void Bullet::set_direction(float mouse_x, float mouse_y) {
  dx = mouse_x - x;
  dy = mouse_y - y;
  float speed = sqrt(dx * dx + dy * dy);
  dx *= 5/speed;
  dy *= 5/speed;
  rotation = (atan2(dy, dx)) * 180 / M_PI;
}

void Bullet::update(float time) {
  x += dx * time;
  y += dy * time;
  life_time += time;
  life = !(check_collision_with_map(dx * time, dy * time));
  if (life_time > BULLET_LIFE_TIME) {
    life = false;
    life_time = 0;
  }
}

void BulletsVector::write_bullets_in_mes(pack::FromServ* message) {
  for (int i = 0; i < bullets.size(); i++) {
    pack::ServObjInfo* cell = message->add_serv_obj_info();
    cell->set_type(pack::ServObjInfo::BULLET);
    cell->set_life(bullets[i]->life);
    cell->set_x(bullets[i]->x);
    cell->set_y(bullets[i]->y);
    cell->set_rotation(bullets[i]->rotation);
    cell->set_id(bullets[i]->id);
    if (!(bullets[i]->life)) {
      bullets.erase(bullets.begin() + i);
    }
  }
}

void BulletsVector::update(float time) {
  for (int i = 0; i < bullets.size(); i++) {
    bullets[i]->update(time);
  }
}

void check_collision_zombie_x_bullet(ZombiesVector* zombie_vec, BulletsVector* bullet_vec,
                                     Client* client_1, Client* client_2) {
  for (int i = 0; i < bullet_vec->bullets.size(); i++) {
    for (int j = 0; j < zombie_vec->zombies.size(); j++) {
      if (bullet_vec->bullets[i]->getRect().intersects(zombie_vec->zombies[j]->getRect())) {
        if (bullet_vec->bullets[i]->id % 2) {
          client_1->points++;
        } else {
          if (!(bullet_vec->bullets[i]->id % 2)) {
            client_2->points++;
          }
        }
        bullet_vec->bullets[i]->life = false;
        zombie_vec->zombies[j]->life = false;
      }
    }
  }
}