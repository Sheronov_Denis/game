#include <math.h>

#include "entity.h"

#define  BULLET_W 16
#define  BULLET_H 3
#define  DIS_STEP 1

void Client::Weapon::calc_bullets(Client* client, BulletsVector* bullets, float mouse_x, float mouse_y, float time) {
  int local_disp = dispersion;
  if (ready_fire) {
    for (int i = 0; i < n_bullets; i++) {
      client->attack = true;
      std::unique_ptr<Bullet> bullet;
      if ((client->rotation > -90) && (client->rotation < 0)) {
        bullet = std::make_unique<Bullet>(client->x, client->y + client->width/2 + 2, BULLET_W, BULLET_H);
      } else if ((client->rotation > -180) && (client->rotation < -90)) {
        bullet = std::make_unique<Bullet>(client->x, client->y + 2, BULLET_W, BULLET_H);
      } else if ((client->rotation > 0) && (client->rotation < 90)) {
        bullet = std::make_unique<Bullet>(client->x, client->y + client->width/2, BULLET_W, BULLET_H);
      } else {
        bullet = std::make_unique<Bullet>(client->x, client->y + 2, BULLET_W, BULLET_H);
      }
      bullets->id++;
      if (client->id == 0) {
        if (!(bullets->id % 2)) {
          bullets->id++;
        }
      } else {
        if (bullets->id % 2) {
          bullets->id++;
        }
      }
      bullet->id = bullets->id;
      bullet->set_direction(mouse_x + local_disp, mouse_y + local_disp);
      bullet->set_collisions(lvl);
      bullets->new_bullet(std::move(bullet));
      local_disp += DIS_STEP;
    }
    ready_fire = false;
  } else {
    current_time += time;
    if (current_time > charge_time) {
      ready_fire = true;
      current_time = 0;
    }
  }
}
