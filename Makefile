.PHONY: all clean

game:
	make proto
	mkdir build
	make -C build/ -f ../Makefile client
	make -C build/ -f ../Makefile server
	cp -r client/newAssets build/client/
	cp client/newAssets/map/tmx/mapInMod.tmx build/server/map.tmx

client:
	mkdir client \
	&& cd client \
	&& cmake ../../client \
	&& make

server:
	mkdir server
	make -C server -f ../../server/Makefile_server

proto: proto/game.proto
	protoc --proto_path="proto" --cpp_out="proto" proto/game.proto

clean:
	rm -rf build/
