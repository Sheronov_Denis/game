//
// Created by mrstranger on 17.12.18.
//

#include "C_MainManager.hpp"

C_MainManager::C_MainManager() : isRunning(true) { }

bool C_MainManager::IsRunning() const {
    return isRunning;
}

void C_MainManager::setIsRunning(bool bol) {
    isRunning = bol;
}
