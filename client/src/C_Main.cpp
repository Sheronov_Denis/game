//
// Created by mrstranger on 15.12.18.
//

#include <imgui-SFML.h>
#include "C_Main.hpp"
#include "C_SceneStart.hpp"

C_Main::C_Main() : window(sf::VideoMode::getDesktopMode(), "Game", sf::Style::Fullscreen), isRepeat(false) {
//C_Main::C_Main() : window(sf::VideoMode(1280, 720), "Game", sf::Style::Default), isRepeat(false) {
    window.setFramerateLimit(60);
    ImGui::SFML::Init(window);

    auto gameScene = std::make_shared<C_SceneGame>(window, sceneManager, socket, packet);
    auto loadScene = std::make_shared<C_SceneLoad>(window, sceneManager, gameScene, socket, packet);
    auto startScene = std::make_shared<C_SceneStart>(sceneManager, loadScene, socket);

    startSceneID = sceneManager.Add(startScene);
    loadSceneID= sceneManager.Add(loadScene);
    gameSceneID = sceneManager.Add(gameScene);

    startScene->SetSwitchToScene(loadSceneID);
    startScene->SetPrevSwitchToScene(gameSceneID);

    loadScene->SetSwitchToScene(gameSceneID);
    loadScene->SetPrevSwitchToScene(startSceneID);

    gameScene->SetSwitchToScene(startSceneID);

    sceneManager.SwitchTo(startSceneID);
//    sceneManager.SwitchTo(gameSceneID);
    deltaTime = clock.restart().asMicroseconds();
    deltaTime /= 500;
}

void C_Main::CaptureInput() {
    sceneManager.ProcessInput();
}

void C_Main::Update() {
    sf::Event event;
    while (window.pollEvent(event)) {
        ImGui::SFML::ProcessEvent(event);

        if (event.type == sf::Event::Closed) {
            window.close();
        }
    }
    ImGui::SFML::Update(window, ImGuiClock.restart());
    sceneManager.Update(deltaTime);
    if (sceneManager.IsRepeat()) {
        isRepeat = true;
    }

    if (!sceneManager.IsOpen()) {
        window.close();
    }
}

void C_Main::LateUpdate() {
    sceneManager.LateUpdate(deltaTime);
}

void C_Main::Draw() {
    sceneManager.Draw(window);
}

void C_Main::CalculateDeltaTime() {
    deltaTime = clock.restart().asMicroseconds();
    deltaTime /= 500;
}

bool C_Main::IsRunning() const {
    return window.isOpen();
}

bool C_Main::IsRepeat() const {
    return isRepeat;
}