//
// Created by mrstranger on 20.12.18.
//

#include "C_SingleSound.hpp"

C_SingleSound& C_SingleSound::Instance() {
    static C_SingleSound singleton;
    return singleton;
}

C_SingleSound::C_SingleSound() {
    soundBufM16 = std::make_shared<sf::SoundBuffer>();
    soundBufM16->loadFromFile("newAssets/sounds/sndUzi.wav");

    soundBufShotgun = std::make_shared<sf::SoundBuffer>();
    soundBufShotgun->loadFromFile("newAssets/sounds/sndShotgun.wav");

    soundBufHit = std::make_shared<sf::SoundBuffer>();
    soundBufHit->loadFromFile("newAssets/sounds/sndHit.wav");

    soundBufGroan = std::make_shared<sf::SoundBuffer>();
    soundBufGroan->loadFromFile("newAssets/sounds/groan.wav");
    soundBufGroan2 = std::make_shared<sf::SoundBuffer>();
    soundBufGroan2->loadFromFile("newAssets/sounds/groan2.wav");
    soundBufGroan3 = std::make_shared<sf::SoundBuffer>();
    soundBufGroan3->loadFromFile("newAssets/sounds/groan3.wav");
    soundBufGroan4 = std::make_shared<sf::SoundBuffer>();
    soundBufGroan4->loadFromFile("newAssets/sounds/groan4.wav");
    soundBufGroan5 = std::make_shared<sf::SoundBuffer>();
    soundBufGroan5->loadFromFile("newAssets/sounds/groan5.wav");
    soundBufGroan6 = std::make_shared<sf::SoundBuffer>();
    soundBufGroan6->loadFromFile("newAssets/sounds/groan6.wav");
}

std::shared_ptr<sf::SoundBuffer>& C_SingleSound::getSoundBufM16() {
    return soundBufM16;
}

std::shared_ptr<sf::SoundBuffer>& C_SingleSound::getSoundBufShotgun() {
    return soundBufShotgun;
}

std::shared_ptr<sf::SoundBuffer>& C_SingleSound::getSoundBufHit() {
    return soundBufHit;
}

std::shared_ptr<sf::SoundBuffer>& C_SingleSound::getSoundBufGroan() {
    return soundBufGroan;
}

std::shared_ptr<sf::SoundBuffer>& C_SingleSound::getSoundBufGroan2() {
    return soundBufGroan2;
}

std::shared_ptr<sf::SoundBuffer>& C_SingleSound::getSoundBufGroan3() {
    return soundBufGroan3;
}

std::shared_ptr<sf::SoundBuffer>& C_SingleSound::getSoundBufGroan4() {
    return soundBufGroan4;
}

std::shared_ptr<sf::SoundBuffer>& C_SingleSound::getSoundBufGroan5() {
    return soundBufGroan5;
}

std::shared_ptr<sf::SoundBuffer>& C_SingleSound::getSoundBufGroan6() {
    return soundBufGroan6;
}