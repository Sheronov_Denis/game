//
// Created by mrstranger on 19.12.18.
//

#include "C_Singletone.hpp"

C_Singletone& C_Singletone::Instance() {
    static C_Singletone singleton;
    return singleton;
}

C_Singletone::C_Singletone() {
    refTexturePlayer = std::make_shared<sf::Texture>();
    refTexturePlayer->loadFromFile("newAssets/animSprites/allPlayerModRez.png");

    refTextureZombie = std::make_shared<sf::Texture>();
    refTextureZombie->loadFromFile("newAssets/animSprites/zombieLegsBlood.png");

    refTextureBullet = std::make_shared<sf::Texture>();
    refTextureBullet->loadFromFile("newAssets/animSprites/sprBullet_strip4.png");
}

std::shared_ptr<sf::Texture>& C_Singletone::getTexturePlayer() {
    return refTexturePlayer;
}

std::shared_ptr<sf::Texture>& C_Singletone::getTextureZombie() {
    return refTextureZombie;
}

std::shared_ptr<sf::Texture>& C_Singletone::getTextureBullet() {
    return refTextureBullet;
}
