#include <SFML/Audio/SoundBuffer.hpp>
#include "C_Player.hpp"

C_Player::C_Player(const std::shared_ptr<sf::Texture>& _texture, int _id, float _x, float _y, int _w, int _h)
                : C_Entity(_texture, _id, _x, _y, _w, _h), points(0), isShooting(false) {
    anim.create("dead", *texture, 0, 0, 60, 60, 5, 0.01, 60, false);
    anim.create("walk", *texture, 0, 92, 44, 32, 8, 0.01, 44, true);
    anim.create("fire", *texture, 0, 124, 44, 32, 5, 0.01, 44, false);
    anim.create("walkm", *texture, 0, 156, 40, 32, 8, 0.01, 44, true);
    anim.create("firem", *texture, 44*5, 124, 40, 32, 2, 0.01, 44, false);

    anim.create("legs", *texture, 0, 60, 32, 32, 16, 0.01, 32, true);

    anim.create("stay", *texture, 0, 92, 44, 32, 1, 0, 0, false);
    anim.create("staym", *texture, 0, 156, 44, 32, 1, 0, 0, false);

    anim.set("staym");
    state = AnimationState::StayM;
    weapon = Weapon::M16;

    for (auto& it : anim.animList) {
        if (it.first != "maskr" || it.first != "maskt") {
            it.second.sprite.setOrigin(16, 16);
        }
    }
}

void C_Player::update(float time) {
    if (state == AnimationState::Stay) anim.set("stay");
    if (state == AnimationState::StayM) anim.set("staym");
    if (state == AnimationState::Walk) anim.set("walk");
    if (state == AnimationState::WalkM) anim.set("walkm");
    if (state == AnimationState::Fire) {
        anim.set("fire");
    }
    if (state == AnimationState::FireM) {
        anim.set("firem");
    }
    if (state == AnimationState::Dead) anim.set("dead");

    anim.tickLegs(time);
    anim.tickMask(time);
    anim.tick(time);
}

void C_Player::checkWeapon(const pack::ServObjInfo& mes) {
    if (weapon == Weapon::M16) {
        if (state == AnimationState::Stay) state = AnimationState::StayM;
        if (state == AnimationState::Walk) state = AnimationState::WalkM;
        if (state == AnimationState::Fire) state = AnimationState::FireM;

        if (mes.attack()) {
            state = AnimationState::FireM;
            anim.play("firem");
        } else if (abs(anim.animList[anim.currentAnim].sprite.getPosition().x - (mes.x() + 8)) > 0
                   || abs(anim.animList[anim.currentAnim].sprite.getPosition().y - (mes.y() + 8)) > 0) {
            if (state != AnimationState::FireM) {
                state = AnimationState::WalkM;
                anim.play("legs");
                anim.play("walkm");
            } else if (!anim.isPlaying()) {
                state = AnimationState::WalkM;
                anim.play("legs");
                anim.play("walkm");
            }
        } else {
            if (state != AnimationState::FireM) {
                state = AnimationState::StayM;
                anim.pause("legs");
                anim.play("staym");
            } else if (!anim.isPlaying()) {
                state = AnimationState::StayM;
                anim.pause("legs");
                anim.play("staym");
            }
        }
    } else if (weapon == Weapon::Drob) {
        if (state == AnimationState::StayM) state = AnimationState::Stay;
        if (state == AnimationState::WalkM) state = AnimationState::Walk;
        if (state == AnimationState::FireM) state = AnimationState::Fire;

        if (mes.attack()) {
            state = AnimationState::Fire;
            anim.play("fire");
        } else if (abs(anim.animList[anim.currentAnim].sprite.getPosition().x - (mes.x() + 8)) > 0
                   || abs(anim.animList[anim.currentAnim].sprite.getPosition().y - (mes.y() + 8)) > 0) {
            if (state != AnimationState::Fire) {
                state = AnimationState::Walk;
                anim.play("legs");
                anim.play("walk");
            } else if (!anim.isPlaying()) {
                state = AnimationState::Walk;
                anim.play("legs");
                anim.play("walk");
            }
        } else {
            if (state != AnimationState::Fire) {
                state = AnimationState::Stay;
                anim.pause("legs");
                anim.play("stay");
            } else if (!anim.isPlaying()) {
                state = AnimationState::Stay;
                anim.pause("legs");
                anim.play("stay");
            }
        }
    }
}

void C_Player::checkStates(const pack::ServObjInfo& mes) {
    if (mes.life()) {
        anim.play("mask");
        checkWeapon(mes);
    } else {
        if (state != AnimationState::Dead) {
            state = AnimationState::Dead;
            anim.pause("legs");
            anim.play("dead");
        }
    }
}
