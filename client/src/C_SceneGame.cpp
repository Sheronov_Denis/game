//
// Created by mrstranger on 15.12.18.
//

#include "C_SceneGame.hpp"

#define TILE_WIDTH 8
#define TILE_HEIGHT 8
#define TILE_COUNT_X 160
#define TILE_COUNT_Y 90

C_SceneGame::C_SceneGame(sf::RenderWindow& _window, C_SceneManager& c_sceneManager, sf::TcpSocket& _socket, sf::Packet& _packet) : moves(0), mousePos(0, 0), window(_window)
                            , sceneManager(c_sceneManager), socket(_socket), packet(_packet), isOverlay(false), window_flags(0) {
    packet.clear();
}

void C_SceneGame::OnCreate() {
    texture.create(TILE_COUNT_X*TILE_WIDTH, TILE_COUNT_Y*TILE_HEIGHT);

    cursorTexture.loadFromFile("newAssets/sprCursor.png");
    cursorSprite.setTexture(cursorTexture);
    cursorSprite.setOrigin(static_cast<float>(CURSOR_WIDTH)/2, static_cast<float>(CURSOR_HEIGHT)/2);
    cursorSprite.setPosition(mousePos.x + static_cast<float>(CURSOR_WIDTH)/2, mousePos.y + static_cast<float>(CURSOR_HEIGHT)/2);

    map = std::make_unique<C_Map>("newAssets/map/tmx/mapInMod.tmx");

    sf::Vector2f pl1;
    sf::Vector2f pl2;
    tmx::ObjectGroup playerObj(map->playersObjGroup);
    for (auto& it : playerObj.GetVec()) {
        if (it.GetObjName() == "player1") {
            pl1.x = it.GetObjX();
            pl1.y = it.GetObjY();
        }
        if (it.GetObjName() == "player2") {
            pl2.x = it.GetObjX();
            pl2.y = it.GetObjY();
        }
    }

    player1 = std::make_unique<C_Player>(C_Singletone::Instance().getTexturePlayer(), 0, 0, 0, 512, 192);
    player1->textureMask.loadFromFile("newAssets/animSprites/masks.png");
    player1->anim.create("maskt", player1->textureMask, 28*3, 28, 28, 28, 1, 0, 0, false);
    player1->anim.animList["maskt"].sprite.setOrigin(18, 12);
    player1->anim.create("maskr", player1->textureMask, 28*3, 0, 28, 28, 1, 0, 0, false);
    player1->anim.animList["maskr"].sprite.setOrigin(18, 12);
    for (auto& it : player1->anim.animList) {
        it.second.sprite.setPosition(pl1.x + static_cast<float>(ENTITY_WIDTH)/2, pl1.y + static_cast<float>(ENTITY_HEIGHT)/2);
        it.second.sprite.setRotation(0);
    }

    player2 = std::make_unique<C_Player>(C_Singletone::Instance().getTexturePlayer(), 1, 0, 0, 512, 192);
    player2->textureMask.loadFromFile("newAssets/animSprites/masks.png");
    player2->anim.create("maskt", player2->textureMask, 28*3, 28, 28, 28, 1, 0, 0, false);
    player2->anim.animList["maskt"].sprite.setOrigin(18, 12);
    player2->anim.create("maskr", player2->textureMask, 28*3, 0, 28, 28, 1, 0, 0, false);
    player2->anim.animList["maskr"].sprite.setOrigin(18, 12);
    for (auto& it : player2->anim.animList) {
        it.second.sprite.setPosition(pl1.x + static_cast<float>(ENTITY_WIDTH)/2, pl1.y + static_cast<float>(ENTITY_HEIGHT)/2);
        it.second.sprite.setRotation(0);
    }

    sf::Vector2f zomb;
    tmx::ObjectGroup zombObj(map->zombiesObjGroup);
    int iterId = 0;
    for (auto& it : zombObj.GetVec()) {
        if (it.GetObjName() == "zombie") {
            zomb.x = it.GetObjX();
            zomb.y = it.GetObjY();
            zombies.push_back(std::make_unique<C_Zombie>(C_Singletone::Instance().getTextureZombie(), iterId, 0, 0, 512, 126));
            for (auto& iter : zombies.back()->anim.animList) {
                iter.second.sprite.setPosition(zomb.x + static_cast<float>(ENTITY_WIDTH)/2, zomb.y + static_cast<float>(ENTITY_HEIGHT)/2);
                iter.second.sprite.setRotation(0);
            }
        }
        iterId++;
    }

    view.reset(sf::FloatRect(0, 0, 640, 360));

    gunTexture.loadFromFile("newAssets/animSprites/sprBigWeapons_strip23.png");
    gunSprite.setTexture(gunTexture);
    gunFrame.emplace_back(sf::IntRect(280, 0, 70, 60));
    gunFrame.emplace_back(sf::IntRect(350, 0, 65, 60));
    gunSprite.setTextureRect(gunFrame.at(0));

    ammoTexture.loadFromFile("newAssets/animSprites/sprAmmoSymbols.png");
    ammoSprite.setTexture(ammoTexture);
    ammoFrame.emplace_back(sf::IntRect(15, 0, 18, 24));
    ammoFrame.emplace_back(sf::IntRect(34, 0, 15, 24));
    ammoSprite.setTextureRect(ammoFrame.at(0));

    infTexture.loadFromFile("newAssets/animSprites/infinity.png");
    infSprite.setTexture(infTexture);

    window_flags |= ImGuiWindowFlags_NoTitleBar;
    window_flags |= ImGuiWindowFlags_NoScrollbar;
    window_flags |= ImGuiWindowFlags_NoMove;
    window_flags |= ImGuiWindowFlags_NoResize;
    window_flags |= ImGuiWindowFlags_NoCollapse;
    window_flags |= ImGuiWindowFlags_NoNav;
    window_flags |= ImGuiWindowFlags_NoBackground;

    window_flags |= ImGuiWindowFlags_NoNavInputs;
    window_flags |= ImGuiWindowFlags_NoNavFocus;
    window_flags |= ImGuiWindowFlags_NoDecoration;
    window_flags |= ImGuiWindowFlags_NoInputs;
    open = true;
    p_open = &open;

    player1->curSound.setBuffer(*C_SingleSound::Instance().getSoundBufM16());
    player2->curSound.setBuffer(*C_SingleSound::Instance().getSoundBufM16());

    soundGroan.setBuffer(*C_SingleSound::Instance().getSoundBufGroan());

    theme.openFromFile("newAssets/sounds/themeResLong.ogg");
}

void C_SceneGame::OnDestroy() {
    isOpen = false;
}

void C_SceneGame::OnActivate() {
    socket.setBlocking(true);
    socket.receive(packet);
    packet >> player1->id >> player2->id;
    packet.clear();

    window.setMouseCursorVisible(false);

    theme.play();
    theme.setLoop(true);
    float curVol = theme.getVolume();
    theme.setVolume(0.5f*curVol);
}

void C_SceneGame::HandleInput() {
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
        isOverlay = true;
    }
    if (!isOverlay) {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
            moves |= 1;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
            moves |= 1 << 1;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
            moves |= 1 << 2;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
            moves |= 1 << 3;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1)) {
            moves |= 1 << 5;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2)) {
            moves |= 1 << 6;
        }
        if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            moves |= 1 << 4;
        }
        sf::Vector2i pixelPos = sf::Mouse::getPosition(window);
        mousePos = window.mapPixelToCoords(pixelPos);
    }
}

void C_SceneGame::SetCoordView(float x, float y) {
    float tempX = x;
    float tempY = y;

    if (x < 320) tempX = 320;
    if (x > 960) tempX = 960;
    if (y < 180) tempY = 180;
    if (y > 540) tempY = 540;

    view.setCenter(tempX, tempY);
}

void C_SceneGame::PackAndSendData() {
    cliInfo.set_mouse_x(mousePos.x + static_cast<float>(5.5));
    cliInfo.set_mouse_y(mousePos.y + static_cast<float>(5.5));
    cliInfo.set_bitmask(moves);
    moves = 0;

    packet << serialiseCliToStr(cliInfo);
    sf::Socket::Status status = socket.send(packet);
    if (status != sf::Socket::Done) {
        std::cerr << "lost send" << std::endl;
    }
    packet.clear();
}

void C_SceneGame::ReceiveData() {
    sf::Socket::Status status = socket.receive(packet);
    if (status != sf::Socket::Done) {
        std::cerr << "lost receive" << std::endl;
    }
    std::string strFrom;
    packet >> strFrom; // read serialised data to strFrom
    parseFromToMsgFrom(strFrom); // parse strFrom to msgFrom
    packet.clear();
}

void C_SceneGame::Update(float deltaTime) {
    PackAndSendData();
    ReceiveData();
    for (int i = 0; i < msgFrom.serv_obj_info().size(); i++) {
        const pack::ServObjInfo& mes = msgFrom.serv_obj_info(i);
        switch (mes.type()) {
            case pack::ServObjInfo::CLIENT: {
                if (mes.id() == player1->id) {
                    if (mes.w_type() == pack::ServObjInfo::M16) {
                        player1->weapon = Weapon::M16;
                    } else if (mes.w_type() == pack::ServObjInfo::SHOTGUN) {
                        player1->weapon = Weapon::Drob;
                    }

                    if (player1->id == 0) {
                        player1->mask = Masks::Rabbit;
                    } else if (player1->id == 1) {
                        player1->mask = Masks::Tiger;
                    }

                    player1->checkStates(mes);

                    for (auto& it : player1->anim.animList) {
                        if (((mes.rotation() < -90 && mes.rotation() > -180) || (mes.rotation() > 90 && mes.rotation() < 180)) && it.first == "legs") {
                            it.second.sprite.setPosition(mes.x() + static_cast<float>(ENTITY_WIDTH)/2, mes.y() + static_cast<float>(ENTITY_HEIGHT)/2 - 4);
                        } else if (it.first != "maskr" || it.first != "maskt") {
                            it.second.sprite.setPosition(mes.x() + static_cast<float>(ENTITY_WIDTH)/2, mes.y() + static_cast<float>(ENTITY_HEIGHT)/2);
                        } else {
                            it.second.sprite.setPosition(mes.x()+static_cast<float>(ENTITY_WIDTH)/2, mes.y()+static_cast<float>(ENTITY_WIDTH)/2);
                        }
                        it.second.sprite.setRotation(mes.rotation());
                    }

                    player1->points = mes.points();
                    if (mes.attack()) {
                        if (player1->weapon == Weapon::M16) {
                            player1->curSound.setBuffer(*C_SingleSound::Instance().getSoundBufM16());
                        } else if (player1->weapon == Weapon::Drob) {
                            player1->curSound.setBuffer(*C_SingleSound::Instance().getSoundBufShotgun());
                        }
                        player1->curSound.play();
                    }
                }

                if (mes.id() == player2->id) {
                    if (mes.w_type() == pack::ServObjInfo::M16) {
                        player2->weapon = Weapon::M16;
                    } else if (mes.w_type() == pack::ServObjInfo::SHOTGUN) {
                        player2->weapon = Weapon::Drob;
                    }

                    if (player2->id == 0) {
                        player2->mask = Masks::Rabbit;
                    } else if (player2->id == 1) {
                        player2->mask = Masks::Tiger;
                    }

                    player2->checkStates(mes);

                    for (auto& it : player2->anim.animList) {
                        if (((mes.rotation() < -90 && mes.rotation() > -180) || (mes.rotation() > 90 && mes.rotation() < 180)) && it.first == "legs") {
                            it.second.sprite.setPosition(mes.x() + static_cast<float>(ENTITY_WIDTH)/2, mes.y() + static_cast<float>(ENTITY_HEIGHT)/2 - 4);
                        } else if (it.first != "maskr" || it.first != "maskt") {
                            it.second.sprite.setPosition(mes.x() + static_cast<float>(ENTITY_WIDTH)/2, mes.y() + static_cast<float>(ENTITY_HEIGHT)/2);
                        } else {
                            it.second.sprite.setPosition(mes.x()+8, mes.y()+8);
                        }
                        it.second.sprite.setRotation(mes.rotation());
                    }

                    player2->points = mes.points();

                    if (mes.attack()) {
                        if (player2->weapon == Weapon::M16) {
                            player2->curSound.setBuffer(*C_SingleSound::Instance().getSoundBufM16());
                        } else if (player2->weapon == Weapon::Drob) {
                            player2->curSound.setBuffer(*C_SingleSound::Instance().getSoundBufShotgun());
                        }
                        player2->curSound.play();
                    }
                }

                break;
            }
            case pack::ServObjInfo::BULLET: {
                if (mes.life()) {
                    if (bullets.empty()) {
                        bullets.emplace_back(std::make_unique<C_Bullet>(C_Singletone::Instance().getTextureBullet(), mes.id(), 0, 0, 64, 3));

                        bullets.back()->anim.play("fly");

                        bullets.back()->setSpriteOnPos(mes.x() + BULLET_WIDTH*0.5f, mes.y() + BULLET_HEIGHT*0.5f);
                        bullets.back()->setSpriteRotation(mes.rotation());

                        for (auto &it : bullets.back()->anim.animList) {
                            it.second.sprite.setPosition(mes.x() + BULLET_WIDTH*0.5f, mes.y() + BULLET_HEIGHT*0.5f);
                            it.second.sprite.setRotation(mes.rotation());
                        }
                    } else {
                        int idx = findBulletById(bullets, mes.id());
                        if (idx == -1) {
                            bullets.emplace_back(std::make_unique<C_Bullet>(C_Singletone::Instance().getTextureBullet(), mes.id(), 0, 0, 64, 3));

                            bullets.back()->anim.play("fly");

                            bullets.back()->setSpriteOnPos(mes.x() + BULLET_WIDTH*0.5f, mes.y() + BULLET_HEIGHT*0.5f);
                            bullets.back()->setSpriteRotation(mes.rotation());

                            for (auto &it : bullets.back()->anim.animList) {
                                it.second.sprite.setPosition(mes.x() + BULLET_WIDTH*0.5f, mes.y() + BULLET_HEIGHT*0.5f);
                                it.second.sprite.setRotation(mes.rotation());
                            }
                        } else {
                            bullets.at(static_cast<unsigned int>(idx))->setSpriteOnPos(mes.x() + BULLET_WIDTH*0.5f, mes.y() + BULLET_HEIGHT*0.5f);
                            bullets.at(static_cast<unsigned int>(idx))->setSpriteRotation(mes.rotation());

                            for (auto &it : bullets.at(static_cast<unsigned int>(idx))->anim.animList) {
                                it.second.sprite.setPosition(mes.x() + BULLET_WIDTH*0.5f, mes.y() + BULLET_HEIGHT*0.5f);
                                it.second.sprite.setRotation(mes.rotation());
                            }
                        }
                    }
                } else {
                    auto idx = findBulletById(bullets, mes.id());
                    if (idx != -1) {
                        bullets.erase(bullets.begin() + idx);
                    }
                }

                break;
            }
            case pack::ServObjInfo::ZOMBIE: {
                int idx = findZombieById(zombies, mes.id());
                if (idx == -1) {
                    zombies.emplace_back(std::make_unique<C_Zombie>(C_Singletone::Instance().getTextureZombie(), mes.id(), 0, 0, 512, 126));
                    zombies.back()->checkStates(mes);

                    for (auto &it : zombies.back()->anim.animList) {
                        it.second.sprite.setPosition(mes.x() + static_cast<float>(ENTITY_WIDTH) / 2, mes.y() + static_cast<float>(ENTITY_HEIGHT) / 2);
                        it.second.sprite.setRotation(mes.rotation());
                    }
                } else {
                    zombies.at(idx)->checkStates(mes);

                    for (auto &it : zombies.at(idx)->anim.animList) {
                        it.second.sprite.setPosition(mes.x() + static_cast<float>(ENTITY_WIDTH) / 2, mes.y() + static_cast<float>(ENTITY_HEIGHT) / 2);
                        it.second.sprite.setRotation(mes.rotation());
                    }
                }

                break;
            }
            default: {
                break;
            }
        }
    }



    cursorSprite.setPosition(mousePos.x + static_cast<float>(5.5), mousePos.y + static_cast<float>(5.5));
    SetCoordView(player1->anim.animList[player1->anim.currentAnim].sprite.getPosition().x, player1->anim.animList[player1->anim.currentAnim].sprite.getPosition().y);
//    SetCoordMapView(player1->anim.animList[player1->anim.currentAnim].sprite.getPosition().x, player1->anim.animList[player1->anim.currentAnim].sprite.getPosition().y);

    player1->update(deltaTime);
    player2->update(deltaTime);

    if (player1->state != AnimationState::Dead || player2->state != AnimationState::Dead) {
        for (auto& it : zombies) {
            it->update(deltaTime);

            if (it->lifeTime > LIFETIME && it->state == AnimationState::Dead) {
                it->isErase = true;
            }
        }
    }

    zombies.erase(std::remove_if(zombies.begin(), zombies.end(),
            [](const std::unique_ptr<C_Zombie> & o) { return o->isErase; }),
                                   zombies.end());

    for (auto& it : bullets) {
        it->update(deltaTime);
    }

    if (globalTime > 1e9) {
        globalTime = 0;
    }
    globalTime += deltaTime;

//    std::mt19937 generator;
//    std::uniform_int_distribution<int> distribution(1, 6);
//    int gen = distribution(generator);
//    switch (gen) {
//        case 1: {
//            soundGroan.setBuffer(*C_SingleSound::Instance().getSoundBufGroan());
//            break;
//        }
//        case 2: {
//            soundGroan.setBuffer(*C_SingleSound::Instance().getSoundBufGroan2());
//            break;
//        }
//        case 3: {
//            soundGroan.setBuffer(*C_SingleSound::Instance().getSoundBufGroan3());
//            break;
//        }
//        case 4: {
//            soundGroan.setBuffer(*C_SingleSound::Instance().getSoundBufGroan4());
//            break;
//        }
//        case 5: {
//            soundGroan.setBuffer(*C_SingleSound::Instance().getSoundBufGroan5());
//            break;
//        }
//        case 6: {
//            soundGroan.setBuffer(*C_SingleSound::Instance().getSoundBufGroan6());
//        }
//        default: {
//            break;
//        }
//    }
//
//    std::mt19937 generatorTime(deltaTime);
//    std::uniform_int_distribution<int> distributionTime(1, 30);
//    int genTime = distributionTime(generatorTime);
//    std::cout << genTime << std::endl;
//    if (genTime == 3) {
//        soundGroan.play();
//    }

    if (static_cast<int>(globalTime) % 700 == 0) {
        soundGroan.setBuffer(*C_SingleSound::Instance().getSoundBufGroan());
        soundGroan.play();
    } else if (static_cast<int>(globalTime) % 600 == 0) {
        soundGroan.setBuffer(*C_SingleSound::Instance().getSoundBufGroan2());
        soundGroan.play();
    } else if (static_cast<int>(globalTime) % 500 == 0) {
        soundGroan.setBuffer(*C_SingleSound::Instance().getSoundBufGroan3());
        soundGroan.play();
    } else if (static_cast<int>(globalTime) % 400 == 0) {
        soundGroan.setBuffer(*C_SingleSound::Instance().getSoundBufGroan4());
        soundGroan.play();
    } else if (static_cast<int>(globalTime) % 300 == 0) {
        soundGroan.setBuffer(*C_SingleSound::Instance().getSoundBufGroan5());
        soundGroan.play();
    } else if (static_cast<int>(globalTime) % 200 == 0) {
        soundGroan.setBuffer(*C_SingleSound::Instance().getSoundBufGroan6());
        soundGroan.play();
    }
}

void C_SceneGame::LateUpdate(float deltaTime) {
    if (player1->weapon == Weapon::M16) {
        gunSprite.setTextureRect(gunFrame.at(0));
        ammoSprite.setTextureRect(ammoFrame.at(0));
    } else if (player1->weapon == Weapon::Drob) {
        gunSprite.setTextureRect(gunFrame.at(1));
        ammoSprite.setTextureRect(ammoFrame.at(1));
    }

    std::string s1 = std::to_string(player1->points*100);
    std::string s2 = std::to_string(player2->points*100);

    sf::Vector2f windowSize(window.getSize());
    ImGui::SetNextWindowSize(ImVec2(windowSize.x, windowSize.y));
    ImGuiIO& io = ImGui::GetIO();
    ImGuiCond c = 0;
    ImGui::SetNextWindowPos(ImVec2(io.DisplaySize.x * 0.5f, io.DisplaySize.y * 0.5f - 100), c, ImVec2(0.5f, 0.5f));

    ImGui::Begin(window_title, p_open, window_flags);

    ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 5.f);
    ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(7,10));
    ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(7,10));

    if (ImGui::BeginPopupModal("Menu", NULL, ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoMove)) {
        window.setMouseCursorVisible(true);
        if (ImGui::Button("Continue", ImVec2(150, 0))) {
            ImGui::CloseCurrentPopup();
            isOverlay = false;
            window.setMouseCursorVisible(false);
        }
        if (ImGui::Button("Exit", ImVec2(150, 0))) {
            OnDestroy();
        }
        ImGui::SetItemDefaultFocus();
        ImGui::EndPopup();
    }

    if (ImGui::BeginPopupModal("Over", NULL, ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoMove)) {
        window.setMouseCursorVisible(true);
        ImFont* font24 = ImGui::GetIO().Fonts->Fonts[1];
        ImGui::PushFont(font24);

        ImGui::Text("Game over!");

        ImGui::Text("Your points: ");
        ImGui::SameLine();
        ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), s1.data());

        ImGui::Text("Your friend's points: ");
        ImGui::SameLine();
        ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), s2.data());
        ImGui::PopFont();

        ImGui::Separator();
        if (ImGui::Button("Main menu", ImVec2(300, 0))) {
            ImGui::CloseCurrentPopup();
            sceneManager.setIsRepeat(true);
            OnDestroy();
        }
        ImGui::SameLine();
        ImGui::PushItemWidth(-1);
        if (ImGui::Button("Exit", ImVec2(150, 0))) {
            OnDestroy();
        }
        ImGui::SetItemDefaultFocus();
        ImGui::EndPopup();
    }

    if (player1->state == AnimationState::Dead && player2->state == AnimationState::Dead) {
        isOverlay = true;
    }

    if (isOverlay && player1->state == AnimationState::Dead && player2->state == AnimationState::Dead) {
        ImGui::OpenPopup("Over");
    } else if (isOverlay) {
        ImGui::OpenPopup("Menu");
    }

    sf::View view = window.getDefaultView();

    //******************ammo
    ImGui::SetCursorPos(ImVec2(5, view.getSize().y-48*1.25f+3));
    ImGui::Image(ammoSprite, sf::Vector2f(18*1.25f, 24*1.25f));

    //******************inf
    ImGui::SetCursorPos(ImVec2(35, view.getSize().y-50));
    ImGui::Image(infSprite, sf::Vector2f(35, 20), sf::Color(255, 241, 6));

    //******************Gun
    ImGui::SetCursorPos(ImVec2(90, view.getSize().y-120/1.5f));
    ImGui::Image(gunSprite, sf::Vector2f(170/1.5f, 120/1.5f));



    // *****************Points
    ImFont* font = ImGui::GetIO().Fonts->Fonts[2];
    ImGui::PushFont(font);
    ImGui::SetCursorPos(ImVec2(view.getSize().x - 230, 40));
    ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), s1.data());
    ImGui::SameLine();
    ImGui::PopFont();
    ImGui::Text("pts");
    //******************

    ImGui::PopStyleVar();
    ImGui::PopStyleVar();
    ImGui::PopStyleVar();
    ImGui::End();
}

void C_SceneGame::PreDraw() {
    texture.clear();
    texture.draw(*(map->c_map));
    texture.display();
}

void C_SceneGame::Draw(sf::RenderWindow& window) {
    PreDraw();
    window.clear(sf::Color(124, 124, 124, 55));
    window.setView(view);
    sf::Sprite map(texture.getTexture());
    window.draw(map);

    DrawZIndex(window);

    if (!bullets.empty()) {
        for (auto &it: bullets) {
            it->anim.draw(window);
        }
    }
//    window.display();
//    ***************mini map
//    PreDraw();
//    window.setView(nmapView);
//    window.draw(map);
//    DrawZIndex(window);
//    window.setView(view);

    window.draw(cursorSprite);
    ImGui::SFML::Render(window);
    window.display();

}

void C_SceneGame::DrawZIndex(sf::RenderWindow& window) {
    if (!zombies.empty()) {
        if (player1->state == AnimationState::Dead && player2->state == AnimationState::Dead) {
            player2->anim.draw(window);
            player1->anim.draw(window);

            for (auto &it : zombies) {
                if (it->state == AnimationState::Dead) {
                    it->anim.draw(window);
                    it->anim.drawBlood(window);
                }
            }
            for (auto &it : zombies) {
                if (it->state != AnimationState::Dead) {
                    if (it->anim.animList["legs"].isPlaying) {
                        it->anim.drawLegs(window);
                    }
                    it->anim.draw(window);
                }
            }
        } else if (player1->state == AnimationState::Dead) {
            player1->anim.draw(window);

            for (auto &it : zombies) {
                if (it->state == AnimationState::Dead) {
                    it->anim.draw(window);
                    it->anim.drawBlood(window);
                }
            }
            for (auto &it : zombies) {
                if (it->state != AnimationState::Dead) {
                    if (it->anim.animList["legs"].isPlaying) {
                        it->anim.drawLegs(window);
                    }
                    it->anim.draw(window);
                }
            }


            if (player2->anim.animList["legs"].isPlaying) {
                player2->anim.drawLegs(window);
            }

            player2->anim.draw(window);
            if (player2->mask == Masks::Rabbit) {
                player2->anim.drawMaskR(window);
            } else if (player2->mask == Masks::Tiger) {
                player2->anim.drawMaskT(window);
            }
        } else if (player2->state == AnimationState::Dead) {
            player2->anim.draw(window);

            for (auto &it : zombies) {
                if (it->state == AnimationState::Dead) {
                    it->anim.draw(window);
                    it->anim.drawBlood(window);
                }
            }
            for (auto &it : zombies) {
                if (it->state != AnimationState::Dead) {
                    if (it->anim.animList["legs"].isPlaying) {
                        it->anim.drawLegs(window);
                    }
                    it->anim.draw(window);
                }
            }


            if (player1->anim.animList["legs"].isPlaying) {
                player1->anim.drawLegs(window);
            }

            player1->anim.draw(window);
            if (player1->mask == Masks::Rabbit) {
                player1->anim.drawMaskR(window);
            } else if (player1->mask == Masks::Tiger) {
                player1->anim.drawMaskT(window);
            }


        } else {
            for (auto &it : zombies) {
                if (it->state == AnimationState::Dead) {
                    it->anim.draw(window);
                    it->anim.drawBlood(window);
                }
            }
            for (auto &it : zombies) {
                if (it->state != AnimationState::Dead) {
                    if (it->anim.animList["legs"].isPlaying) {
                        it->anim.drawLegs(window);
                    }
                    it->anim.draw(window);
                }
            }

            if (player1->anim.animList["legs"].isPlaying) {
                player1->anim.drawLegs(window);
            }
            if (player2->anim.animList["legs"].isPlaying) {
                player2->anim.drawLegs(window);
            }

            player2->anim.draw(window);
            player1->anim.draw(window);
            if (player1->mask == Masks::Rabbit) {
                player1->anim.drawMaskR(window);
            } else if (player1->mask == Masks::Tiger) {
                player1->anim.drawMaskT(window);
            }
            if (player2->mask == Masks::Rabbit) {
                player2->anim.drawMaskR(window);
            } else if (player2->mask == Masks::Tiger) {
                player2->anim.drawMaskT(window);
            }
        }
    } else {
        player2->anim.draw(window);
        player1->anim.draw(window);

        if (player1->mask == Masks::Rabbit) {
            player1->anim.drawMaskR(window);
        } else if (player1->mask == Masks::Tiger) {
            player1->anim.drawMaskT(window);
        }

        if (player2->mask == Masks::Rabbit) {
            player2->anim.drawMaskR(window);
        } else if (player2->mask == Masks::Tiger) {
            player2->anim.drawMaskT(window);
        }

    }
}

std::string C_SceneGame::serialiseCliToStr(pack::ClientObjInfo& cliInfo) {
    std::string strCli;
    cliInfo.SerializeToString(&strCli);
    return strCli;
}
void C_SceneGame::parseFromToMsgFrom(std::string& from) {
    msgFrom.ParseFromString(from);
}

int C_SceneGame::findBulletById(std::vector<std::unique_ptr<C_Bullet>>& bullets, int id) {
    int idx = 0;
    for (auto& it : bullets) {
        if (it->id == id) {
            return idx;
        }
        idx++;
    }
    return -1;
}

int C_SceneGame::findZombieById(std::vector<std::unique_ptr<C_Zombie>>& zombies, int id) {
    int idx = 0;
    for (auto& it : zombies) {
        if (it->id == id) {
            return idx;
        }
        idx++;
    }
    return -1;
}

void C_SceneGame::setIp(char* _ip) {
    ip = _ip;
}

void C_SceneGame::setPort(char* _port) {
    port = static_cast<ushort>(std::stoul(_port));
}

void C_SceneGame::setIp(std::string& _ip) {
    ip = _ip;
}

void C_SceneGame::setPort(ushort _port) {
    port = _port;
}

void C_SceneGame::SetSwitchToScene(unsigned int id) {
    nextSceneID = id;
}