//
// Created by mrstranger on 02.12.18.
//

#include "C_Map.hpp"

C_Map::C_Map(const std::string& path) {
    c_map = std::make_unique<tmx::TileMap>(path);

    height = c_map->GetWidth();
    width = c_map->GetHeight();
    tileHeight = c_map->GetTileWidth();
    tileWidth = c_map->GetTileHeight();

    playersObjGroup = c_map->GetObjectGroup("player");
    zombiesObjGroup = c_map->GetObjectGroup("zombies");
}
