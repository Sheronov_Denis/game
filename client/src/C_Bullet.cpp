//
// Created by mrstranger on 03.12.18.
//

#include "C_Bullet.hpp"

C_Bullet::C_Bullet(const std::shared_ptr<sf::Texture>& _texture, int _id, float _x, float _y, int _w, int _h) : C_Entity(_texture, _id, _x, _y, _w, _h) {
    //        anim.create("fly", texture, 0, 0, 16, 3, 4, 0.04, 16, false);
    anim.create("fly", *texture, 0, 0, 16, 3, 1, 0, 0, false);
    anim.animList[anim.currentAnim].sprite.setColor(sf::Color::Yellow);
    anim.set("fly");
}

void C_Bullet::update(float time) {
    anim.tick(time);
}
