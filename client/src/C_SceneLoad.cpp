//
// Created by mrstranger on 15.12.18.
//


#include <iostream>
#include <SFML/Network.hpp>
#include "C_SceneLoad.hpp"

C_SceneLoad::C_SceneLoad(sf::RenderWindow& _window, C_SceneManager& _sceneManager, std::shared_ptr<C_SceneGame> _gameScene, sf::TcpSocket& _socket, sf::Packet& _packet) : window(_window), sceneManager(_sceneManager),
                        gameScene(_gameScene), socket(_socket), packet(_packet), window_flags(0), isFullSession(false) {
}

void C_SceneLoad::OnCreate() {
    backgroundTexture.loadFromFile("newAssets/backgrounds/load1.jpg", sf::IntRect(0, 0, 1920, 1080));
    backgroundSprite.setTexture(backgroundTexture);

    window_flags |= ImGuiWindowFlags_NoTitleBar;
    window_flags |= ImGuiWindowFlags_NoScrollbar;
    window_flags |= ImGuiWindowFlags_NoMove;
    window_flags |= ImGuiWindowFlags_NoResize;
    window_flags |= ImGuiWindowFlags_NoCollapse;
    window_flags |= ImGuiWindowFlags_NoNav;
    window_flags |= ImGuiWindowFlags_NoBackground;
    open = true;
    p_open = &open;

    window.setMouseCursorVisible(true);
}

void C_SceneLoad::OnDestroy() {
    isOpen = false;
}

void C_SceneLoad::OnActivate() { }

void C_SceneLoad::HandleInput() { }

void C_SceneLoad::Update(float deltaTime) {
    socket.setBlocking(false);
    socket.receive(packet);
    socket.setBlocking(true);
    packet >> isFullSession;
    packet.clear();

    if (isFullSession) {
        gameScene->setIp(ip);
        gameScene->setPort(port);
        sceneManager.SwitchTo(nextSceneID);
    }

    ImGui::SetNextWindowSize(ImVec2(450, 300));
    ImGuiIO& io = ImGui::GetIO();
    ImGuiCond c = 0;
    ImGui::SetNextWindowPos(ImVec2(io.DisplaySize.x * 0.5f, io.DisplaySize.y * 0.5f), c, ImVec2(0.5f, 0.5f));

    ImGui::Begin(window_title, p_open, window_flags);
    ImFont* font = ImGui::GetIO().Fonts->Fonts[0];
    ImGui::PushFont(font);

    ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(4,10));

    ImGui::Text("Connected");
    ImGui::Text("Waiting for other client...");

    ImGui::PopStyleVar();
    ImGui::PopFont();
    ImGui::End();
}

void C_SceneLoad::LateUpdate(float deltaTime) { }


void C_SceneLoad::Draw(sf::RenderWindow &window) {
    window.clear();

    window.draw(backgroundSprite);
    ImGui::SFML::Render(window);
    window.display();
}

void C_SceneLoad::SetSwitchToScene(unsigned int id) {
    nextSceneID = id;
}

void C_SceneLoad::SetPrevSwitchToScene(unsigned int id) {
    prevSceneID = id;
}

void C_SceneLoad::setIp(char* _ip) {
    ip = _ip;
}

void C_SceneLoad::setPort(char* _port) {
    port = static_cast<ushort>(std::stoul(_port));
}

void C_SceneLoad::setIp(std::string& _ip) {
    ip = _ip;
}

void C_SceneLoad::setPort(ushort _port) {
    port = _port;
}