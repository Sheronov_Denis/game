#include "C_Entity.hpp"

C_Entity::C_Entity(const std::shared_ptr<sf::Texture>& _texture, int _id, float _x, float _y, int _w, int _h) : texture(_texture),
                    id(_id), x(_x), y(_y), width(_w), height(_h), state(AnimationState::None) {
//    texture.loadFromFile(path, sf::IntRect(static_cast<int>(x), static_cast<int>(y), width, height));
//    sprite.setTexture(texture);
    sprite.setOrigin(static_cast<float>(width) / 2, static_cast<float>(height)/ 2);
}

sf::FloatRect C_Entity::getRect() {
    return sf::FloatRect(x, y, width, height);
}

sf::Sprite& C_Entity::getSprite() {
    return sprite;
}

void C_Entity::setSpriteOrigin(float x, float y) {
    sprite.setOrigin(x, y);
}

float C_Entity::getPositionX() {
    return sprite.getPosition().x;
}
float C_Entity::getPositionY() {
    return sprite.getPosition().y;
}

void C_Entity::setSpriteOnPos(float x, float y) {
    sprite.setPosition(x, y);
}
void C_Entity::setSpriteRotation(float rotation) {
    sprite.setRotation(rotation);
}
