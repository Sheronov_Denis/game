//
// Created by mrstranger on 15.12.18.
//


#include "C_SceneStart.hpp"

C_SceneStart::C_SceneStart(C_SceneManager& _sceneManager, std::shared_ptr<C_SceneLoad> _loadScene, sf::TcpSocket& _socket) : window_flags(0), ipBuf(""), portBuf(""), window_title("title"),
                            sceneManager(_sceneManager), loadScene(_loadScene), socket(_socket) { }

void C_SceneStart::OnCreate() {
    backgroundTexture.loadFromFile("newAssets/backgrounds/wall.png", sf::IntRect(0, 0, 1920, 1080));
    backgroundSprite.setTexture(backgroundTexture);

    window_flags |= ImGuiWindowFlags_NoTitleBar;
    window_flags |= ImGuiWindowFlags_NoScrollbar;
    window_flags |= ImGuiWindowFlags_NoMove;
    window_flags |= ImGuiWindowFlags_NoResize;
    window_flags |= ImGuiWindowFlags_NoCollapse;
    window_flags |= ImGuiWindowFlags_NoNav;
    window_flags |= ImGuiWindowFlags_NoBackground;
    open = true;
    p_open = &open;

    ImGui::GetIO().Fonts->Clear(); // clear fonts if you loaded some before (even if only default one was loaded)
    ImGui::GetIO().Fonts->AddFontFromFileTTF("newAssets/fonts/PressStart2P-Regular.ttf", 16.f);
    ImGui::GetIO().Fonts->AddFontFromFileTTF("newAssets/fonts/PressStart2P-Regular.ttf", 24.f);
    ImGui::GetIO().Fonts->AddFontFromFileTTF("newAssets/fonts/PressStart2P-Regular.ttf", 28.f);
    ImGui::SFML::UpdateFontTexture(); // important call: updates font texture
}

void C_SceneStart::OnDestroy() {
    isOpen = false;
}

void C_SceneStart::OnActivate() { }

void C_SceneStart::HandleInput() { }

void C_SceneStart::Update(float deltaTime) {
    ImGui::SetNextWindowSize(ImVec2(340, 250));
    ImGuiIO& io = ImGui::GetIO();
    ImGuiCond c = 0;
    ImGui::SetNextWindowPos(ImVec2(io.DisplaySize.x * 0.5f, io.DisplaySize.y * 0.5f - 100), c, ImVec2(0.5f, 0.5f));

    ImGui::Begin(window_title, p_open, window_flags);

    ImFont* font = ImGui::GetIO().Fonts->Fonts[0];
    ImGui::PushFont(font);

    ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 5.f);
    ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(4,10));
    ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(7,10));

    ImGui::PushItemWidth(290);
    ImGui::InputText("Ip", ipBuf, 255);
    ImGui::PopItemWidth();

    ImGui::PushItemWidth(200);
    ImGui::InputText("Port", portBuf, 255);
    ImGui::PopItemWidth();

    if (ImGui::BeginPopupModal("Incorrect", NULL, ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoMove)) {
        ImGui::Text("Incorrect ip or port! Please, try again...");
        ImGui::Separator();

        if (ImGui::Button("OK", ImVec2(140, 50))) { ImGui::CloseCurrentPopup(); }
        ImGui::SetItemDefaultFocus();
        ImGui::EndPopup();
    }

    if (ImGui::Button("Connect", ImVec2(140, 50))) {
        if (!CheckInput()) {
            ImGui::OpenPopup("Incorrect");
        } else {
            setIp(ipBuf);
            setPort(portBuf);

            loadScene->setIp(ipBuf);
            loadScene->setPort(portBuf);

            sf::Socket::Status status = socket.connect(ip, port);
            if (status != sf::Socket::Done) {
                std::cout << "Not connected" << std::endl;
                ImGui::OpenPopup("Incorrect");
            } else {
                std::cout << "Connected" << std::endl;
                sceneManager.SwitchTo(nextSceneID);
            }
        }
    }

    if (ImGui::Button("Exit", ImVec2(120, 60))) {
        OnDestroy();
    }

    ImGui::PopStyleVar();
    ImGui::PopStyleVar();
    ImGui::PopStyleVar();
    ImGui::PopFont();
    ImGui::End();
}

void C_SceneStart::Draw(sf::RenderWindow &window) {
    window.clear();

    window.draw(backgroundSprite);
    ImGui::SFML::Render(window);
    window.display();
}

bool C_SceneStart::CheckInput() {
    if (ipBuf[0] == '\0' || portBuf[0] == '\0') {
        return false;
    }
    return true;
}

void C_SceneStart::SetSwitchToScene(unsigned int id) {
    nextSceneID = id;
}

void C_SceneStart::SetPrevSwitchToScene(unsigned int id) {
    prevSceneID = id;
}

void C_SceneStart::setIp(char* _ip) {
    ip = _ip;
}

void C_SceneStart::setPort(char* _port) {
    port = static_cast<ushort>(std::stoul(_port));
}
