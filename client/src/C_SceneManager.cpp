//
// Created by mrstranger on 15.12.18.
//

#include "C_SceneManager.hpp"

C_SceneManager::C_SceneManager() : scenes(0), curScene(0), isOpen(true), isRepeat(false) { }

void C_SceneManager::ProcessInput() {
    if(curScene) {
        curScene->HandleInput();
    }
}

void C_SceneManager::Update(float deltaTime) {
    if(curScene) {
        curScene->Update(deltaTime);

        if (!curScene->IsOpen()) {
            isOpen = false;
        }
    }
}

void C_SceneManager::LateUpdate(float deltaTime) {
    if(curScene){
        curScene->LateUpdate(deltaTime);
    }
}

void C_SceneManager::Draw(sf::RenderWindow& window) {
    if(curScene) {
        curScene->Draw(window);
    }
}

unsigned int C_SceneManager::Add(std::shared_ptr<C_Scene> scene) {
    auto inserted = scenes.insert(std::make_pair(insertedSceneID, scene));
    inserted.first->second->OnCreate();
    return insertedSceneID++;
}

void C_SceneManager::Remove(unsigned int id) {
    auto it = scenes.find(id);
    if(it != scenes.end()) {
        if(curScene == it->second) {
            curScene = nullptr;
        }
        it->second->OnDestroy();
        scenes.erase(it);
    }
}

void C_SceneManager::SwitchTo(unsigned int id) {
    auto it = scenes.find(id);
    if(it != scenes.end()) {
        if(curScene) {
            curScene->OnDeactivate();
        }
        curScene = it->second;
        curScene->OnActivate();
    }
}

bool C_SceneManager::IsOpen() {
    return isOpen;
}

void C_SceneManager::Close() {
    isOpen = false;
}

void C_SceneManager::setIsRepeat(bool bol) {
    isRepeat = bol;
}

bool C_SceneManager::IsRepeat() {
    return isRepeat;
}
