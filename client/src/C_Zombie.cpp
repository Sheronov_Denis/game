//
// Created by mrstranger on 02.12.18.
//

#include <C_SingleSound.hpp>
#include "C_Zombie.hpp"

C_Zombie::C_Zombie(const std::shared_ptr<sf::Texture>& _texture, int _id, float _x, float _y, int _w, int _h) : C_Entity(_texture, _id, _x, _y, _w, _h), lifeTime(0), isErase(false) {
    anim.create("walk", *texture, 0, 32, 32, 32, 8, 0.01, 32, true);
    anim.create("dead", *texture, 0, 64, 60, 60, 2, 0.01, 60, false);
    anim.create("fire", *texture, 120, 64, 32, 32, 6, 0.01, 32, false);
    anim.create("stay", *texture, 0, 32, 32, 32, 1, 0, 0, false);
    anim.create("legs", *texture, 0, 0, 32, 32, 16, 0.01, 32, true);
    anim.create("blood", *texture, 120, 102, 24, 24, 8, 0.01, 24, false);

    anim.set("stay");
    state = AnimationState::Stay;

    hitSound.setBuffer(*C_SingleSound::Instance().getSoundBufHit());
}

void C_Zombie::update(float time) {
    if (state == AnimationState::Stay) anim.set("stay");
    if (state == AnimationState::Walk) {
        anim.set("walk");
    }
    if (state == AnimationState::Dead) {
        anim.set("dead");
        lifeTime += time;
    }
    if (state == AnimationState::Fire) anim.set("fire");

    if (state != AnimationState::Dead) {
        anim.tickLegs(time);
    }

    anim.tickBlood(time);
    anim.tick(time);
}

void C_Zombie::checkStates(const pack::ServObjInfo& mes) {
    if (mes.life()) {
        if (mes.attack()) {
            state = AnimationState::Fire;
            anim.play("fire");
        } else if (abs(anim.animList[anim.currentAnim].sprite.getPosition().x - (mes.x() + 8)) > 0
                   || abs(anim.animList[anim.currentAnim].sprite.getPosition().y - (mes.y() + 8)) > 0) {
            if (state != AnimationState::Fire) {
                state = AnimationState::Walk;
                anim.play("legs");
                anim.play("walk");
            } else if (!anim.isPlaying()) {
                state = AnimationState::Walk;
                anim.play("legs");
                anim.play("walk");
            }
        } else {
            if (state != AnimationState::Fire) {
                state = AnimationState::Stay;
                anim.pause("legs");
                anim.play("stay");
            } else if (!anim.isPlaying()) {
                state = AnimationState::Stay;
                anim.pause("legs");
                anim.play("stay");
            }
        }
    } else {
        hitSound.play();
        if (state != AnimationState::Dead) {
            state = AnimationState::Dead;
            anim.pause("legs");
            anim.play("blood");
            anim.play("dead");
        }
    }
}
