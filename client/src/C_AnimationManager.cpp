//
// Created by mrstranger on 18.12.18.
//

#include "C_AnimationManager.hpp"

C_AnimationManager::~C_AnimationManager() {
    animList.clear();
}

void C_AnimationManager::create(std::string name, sf::Texture &texture, int x, int y, int w, int h, int count, float speed, int step, bool loop) {
    C_Animation a;
    a.speed = speed;
    a.loop = loop;
    a.sprite.setTexture(texture);
    a.sprite.setOrigin(static_cast<float>(w)/2, static_cast<float>(h)/2);

    for (int i=0;i<count;i++) {
        a.frames.emplace_back( sf::IntRect(x+i*step, y, w, h) );
    }
    animList[name] = a;
    currentAnim = name;
}

void C_AnimationManager::set(std::string name) {
    currentAnim = name;
}

void C_AnimationManager::draw(sf::RenderWindow &window) {
    window.draw(animList[currentAnim].sprite);
}
void C_AnimationManager::drawLegs(sf::RenderWindow& window) {
    animList["legs"].sprite.move(0, 1);
    window.draw(animList["legs"].sprite);
}
void C_AnimationManager::drawBlood(sf::RenderWindow &window) {
    window.draw(animList["blood"].sprite);
}
void C_AnimationManager::drawMaskR(sf::RenderWindow &window) {
    window.draw(animList["maskr"].sprite);
}
void C_AnimationManager::drawMaskT(sf::RenderWindow &window) {
    window.draw(animList["maskt"].sprite);
}

void C_AnimationManager::tick(float time) {
    animList[currentAnim].tick(time);
}
void C_AnimationManager::tickLegs(float time) {
    animList["legs"].tick(time);
}
void C_AnimationManager::tickBlood(float time) {
    animList["blood"].tick(time);
}
void C_AnimationManager::tickMask(float time) {
    animList["maskr"].tick(time);
    animList["maskt"].tick(time);
}

void C_AnimationManager::pause(std::string name) {
    animList[name].isPlaying = false;
}

void C_AnimationManager::play(){
    animList[currentAnim].isPlaying=true;
}

void C_AnimationManager::play(std::string name) {
    animList[name].isPlaying = true;
}

bool C_AnimationManager::isPlaying() {
    return animList[currentAnim].isPlaying;
}
