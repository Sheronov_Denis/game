//
// Created by mrstranger on 09.12.18.
//

#include "C_Animation.hpp"

C_Animation::C_Animation() {
    currentFrame = 0;
    isPlaying = true;
    loop = true;
}

void C_Animation::tick(float time) {
    if (!isPlaying) return;

    currentFrame += speed * time;

    if (currentFrame > frames.size()) {
        currentFrame -= frames.size();
        if (!loop) {
            isPlaying=false;
            return;
        }
    }

    int i = static_cast<int>(currentFrame);
    sprite.setTextureRect( frames[i] );
}
