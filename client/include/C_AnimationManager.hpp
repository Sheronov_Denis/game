//
// Created by mrstranger on 18.12.18.
//

#ifndef OTHER_C_ANIMATIONMANAGER_HPP
#define OTHER_C_ANIMATIONMANAGER_HPP

#include <string>
#include <map>
#include "C_Animation.hpp"

class C_AnimationManager {
public:
    std::string currentAnim;
    std::map<std::string, C_Animation> animList;

    C_AnimationManager() = default;
    ~C_AnimationManager();

    void create(std::string name, sf::Texture &texture, int x, int y, int w, int h, int count, float speed, int step, bool loop);
    void set(std::string name);
    void draw(sf::RenderWindow &window);
    void drawLegs(sf::RenderWindow& window);
    void drawBlood(sf::RenderWindow &window);
    void drawMaskR(sf::RenderWindow &window);
    void drawMaskT(sf::RenderWindow &window);
    void tick(float time);
    void tickLegs(float time);
    void tickBlood(float time);
    void tickMask(float time);
    void pause(std::string name);
    void play();
    void play(std::string name);
    bool isPlaying();
};


#endif //OTHER_C_ANIMATIONMANAGER_HPP
