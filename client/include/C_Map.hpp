//
// Created by mrstranger on 02.12.18.
//

#ifndef OTHER_C_MAP_HPP
#define OTHER_C_MAP_HPP

#include <STP/Core/TileMap.hpp>

class C_Map {
 public:
    std::unique_ptr<tmx::TileMap> c_map;

    tmx::ObjectGroup playersObjGroup;
    tmx::ObjectGroup zombiesObjGroup;

    unsigned int width;
    unsigned int height;
    unsigned int tileWidth;
    unsigned int tileHeight;

    explicit C_Map(const std::string& path);
};


#endif //OTHER_C_MAP_HPP
