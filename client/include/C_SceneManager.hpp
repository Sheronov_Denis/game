//
// Created by mrstranger on 15.12.18.
//

#ifndef OTHER_C_SCENEMANAGER_HPP
#define OTHER_C_SCENEMANAGER_HPP

#include <memory>
#include <unordered_map>

#include "C_Scene.hpp"

class C_SceneManager {
 public:
    C_SceneManager();

    void ProcessInput();
    void Update(float deltaTime);
    void LateUpdate(float deltaTime);
    void Draw(sf::RenderWindow& window);

    unsigned int Add(std::shared_ptr<C_Scene> scene);
    void SwitchTo(unsigned int id);
    void Remove(unsigned int id);

    void setIsRepeat(bool bol);
    bool IsRepeat();

    void Close();
    bool IsOpen();

private:
    std::unordered_map<unsigned int, std::shared_ptr<C_Scene>> scenes;
    std::shared_ptr<C_Scene> curScene;
    unsigned int insertedSceneID;

    bool isOpen;
    bool isRepeat;
};


#endif //OTHER_C_SCENEMANAGER_HPP
