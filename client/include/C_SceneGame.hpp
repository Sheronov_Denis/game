//
// Created by mrstranger on 15.12.18.
//

#ifndef OTHER_C_SCENEGAME_HPP
#define OTHER_C_SCENEGAME_HPP

#include <cmath>
#include <stdlib.h>
#include <game.pb.h>
#include "STP/TMXLoader.hpp"
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/Network.hpp>
#include <SFML/Audio.hpp>
#include <memory>
#include <iostream>
#include "imgui.h"
#include "imgui-SFML.h"

#include "C_Scene.hpp"
#include "C_Map.hpp"
#include "C_Player.hpp"
#include "C_Bullet.hpp"
#include "C_Zombie.hpp"
#include "C_SceneManager.hpp"
#include "C_Singletone.hpp"
#include "C_SingleSound.hpp"

#define ENTITY_WIDTH 16
#define ENTITY_HEIGHT 16
#define CURSOR_WIDTH 11
#define CURSOR_HEIGHT 11
#define BULLET_WIDTH 16
#define BULLET_HEIGHT 3

class C_SceneGame : public C_Scene {
 public:
    C_SceneGame(sf::RenderWindow& _window, C_SceneManager& c_sceneManager, sf::TcpSocket& _socket, sf::Packet& _packet);

    void OnCreate() override;
    void OnDestroy() override;

    void OnActivate() override;

    void SetSwitchToScene(unsigned int id);
    void HandleInput() override;
    void Update(float deltaTime) override;
    void LateUpdate(float deltaTime) override;
    void Draw(sf::RenderWindow& window) override;
    void PreDraw();
    void DrawZIndex(sf::RenderWindow& window);

    void PackAndSendData();
    void ReceiveData();

    std::string serialiseCliToStr(pack::ClientObjInfo& cliInfo);
    void parseFromToMsgFrom(std::string& from);
    int findBulletById(std::vector<std::unique_ptr<C_Bullet>>& bullets, int id);
    int findZombieById(std::vector<std::unique_ptr<C_Zombie>>& zombies, int id);
    void SetCoordView(float x, float y);
//    void SetCoordMapView(float x, float y);

    void setIp(char* ip);
    void setPort(char* _port);

    void setIp(std::string& _ip);
    void setPort(ushort _port);

private:
    uint32_t moves;
    sf::Vector2f mousePos;

    sf::Texture cursorTexture;
    sf::Sprite cursorSprite;

    sf::View view;
    std::unique_ptr<C_Map> map;
    sf::RenderTexture texture;

    pack::FromServ msgFrom;
    pack::ClientObjInfo cliInfo;

    sf::TcpSocket& socket;
    sf::Packet& packet;

    std::unique_ptr<C_Player> player1;
    std::unique_ptr<C_Player> player2;
    std::vector<std::unique_ptr<C_Zombie>> zombies;
    std::vector<std::unique_ptr<C_Bullet>> bullets;

    std::string ip;
    ushort port;

    sf::RenderWindow& window;

    ImGuiWindowFlags window_flags;
    char window_title[16];
    bool open;
    bool* p_open;

    bool isOverlay;

    sf::Texture gunTexture;
    std::vector<sf::IntRect> gunFrame;
    sf::Sprite gunSprite;

    sf::Texture ammoTexture;
    std::vector<sf::IntRect> ammoFrame;
    sf::Sprite ammoSprite;

    sf::Texture infTexture;
    sf::Sprite infSprite;

    sf::View nmapView;
    sf::Sprite nmap;

    C_SceneManager& sceneManager;
    unsigned int nextSceneID;

    sf::Sound soundGroan;

    float globalTime;

    sf::Music theme;
};


#endif //OTHER_C_SCENEGAME_HPP
