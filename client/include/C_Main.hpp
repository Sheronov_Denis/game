//
// Created by mrstranger on 15.12.18.
//

#ifndef OTHER_C_MAIN_HPP
#define OTHER_C_MAIN_HPP

#include <SFML/Graphics.hpp>

#include "C_SceneManager.hpp"
#include "C_SceneStart.hpp"
#include "C_SceneGame.hpp"

class C_Main {
public:
    C_Main();

    void CaptureInput();
    void Update();
    void LateUpdate();
    void Draw();
    void CalculateDeltaTime();
    bool IsRunning() const;

    bool IsRepeat() const;

private:
    sf::RenderWindow window;

    sf::TcpSocket socket;
    sf::Packet packet;

    sf::Clock clock;
    sf::Clock ImGuiClock;
    float deltaTime;

    C_SceneManager sceneManager;
    unsigned int startSceneID;
    unsigned int loadSceneID;
    unsigned int gameSceneID;

    bool isRepeat;

};


#endif //OTHER_C_MAIN_HPP
