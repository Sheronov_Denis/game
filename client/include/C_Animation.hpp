//
// Created by mrstranger on 09.12.18.
//

#ifndef OTHER_C_ANIMATION_HPP
#define OTHER_C_ANIMATION_HPP

#include <SFML/Graphics/Sprite.hpp>
#include <string>
#include <vector>
#include <map>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Texture.hpp>

class C_Animation {
 public:
    std::vector<sf::IntRect> frames;
    float currentFrame, speed;
    bool loop, isPlaying;
    sf::Sprite sprite;

    C_Animation();
    void tick(float time);
};

#endif //OTHER_C_ANIMATION_HPP
