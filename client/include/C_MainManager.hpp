//
// Created by mrstranger on 17.12.18.
//

#ifndef OTHER_C_MAINMANAGER_HPP
#define OTHER_C_MAINMANAGER_HPP

#include <SFML/Graphics/RenderWindow.hpp>

class C_MainManager {
 public:
    C_MainManager();
    bool IsRunning() const;
    void setIsRunning(bool bol);

 private:
    bool isRunning;
};


#endif //OTHER_C_MAINMANAGER_HPP
