//
// Created by mrstranger on 20.12.18.
//

#ifndef CLIENT_OUT_C_SINGLESOUND_HPP
#define CLIENT_OUT_C_SINGLESOUND_HPP

#include <SFML/Audio.hpp>
#include <memory>

class C_SingleSound {
public:
    static C_SingleSound& Instance();

    std::shared_ptr<sf::SoundBuffer>& getSoundBufM16();
    std::shared_ptr<sf::SoundBuffer>& getSoundBufShotgun();
    std::shared_ptr<sf::SoundBuffer>& getSoundBufHit();

    std::shared_ptr<sf::SoundBuffer>& getSoundBufGroan();
    std::shared_ptr<sf::SoundBuffer>& getSoundBufGroan2();
    std::shared_ptr<sf::SoundBuffer>& getSoundBufGroan3();
    std::shared_ptr<sf::SoundBuffer>& getSoundBufGroan4();
    std::shared_ptr<sf::SoundBuffer>& getSoundBufGroan5();
    std::shared_ptr<sf::SoundBuffer>& getSoundBufGroan6();

private:
    C_SingleSound();

    std::shared_ptr<sf::SoundBuffer> soundBufM16;
    std::shared_ptr<sf::SoundBuffer> soundBufShotgun;
    std::shared_ptr<sf::SoundBuffer> soundBufHit;

    std::shared_ptr<sf::SoundBuffer> soundBufGroan;
    std::shared_ptr<sf::SoundBuffer> soundBufGroan2;
    std::shared_ptr<sf::SoundBuffer> soundBufGroan3;
    std::shared_ptr<sf::SoundBuffer> soundBufGroan4;
    std::shared_ptr<sf::SoundBuffer> soundBufGroan5;
    std::shared_ptr<sf::SoundBuffer> soundBufGroan6;
};


#endif //CLIENT_OUT_C_SINGLESOUND_HPP
