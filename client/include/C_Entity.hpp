#ifndef C_ENTITY_HPP
#define C_ENTITY_HPP

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <string>
#include <iostream>
#include "game.pb.h"
#include "C_Animation.hpp"
#include "C_AnimationManager.hpp"


enum class AnimationState {
    None,
    Stay,
    StayM,
    Walk,
    WalkM,
    Fire,
    FireM,
    Dead
};

class C_Entity {
 public:
    C_AnimationManager anim;
    AnimationState state;

    std::shared_ptr<sf::Texture> texture;
//    sf::Texture texture;
    sf::Sprite sprite;
    int id;

    float x;
    float y;
    int width;
    int height;

//    C_Entity(const std::string& path, int _id, int _x, int _y, int _w, int _h);
    C_Entity(const std::shared_ptr<sf::Texture>& _texture, int _id, float _x, float _y, int _w, int _h);
    virtual ~C_Entity() = default;
    virtual void update(float time) = 0;

    sf::FloatRect getRect();
    sf::Sprite& getSprite();
    void setSpriteOrigin(float x, float y);
    float getPositionX();
    float getPositionY();
    void setSpriteOnPos(float x, float y);
    void setSpriteRotation(float rotation);
};


#endif  // C_ENTITY_HPP