//
// Created by mrstranger on 03.12.18.
//

#ifndef OTHER_C_BULLET_HPP
#define OTHER_C_BULLET_HPP

#include "C_Entity.hpp"

class C_Bullet : public C_Entity {
 public:
    C_Bullet(const std::shared_ptr<sf::Texture>& _texture, int _id, float _x, float _y, int _w, int _h);
    void update(float time) override;
};


#endif //OTHER_C_BULLET_HPP
