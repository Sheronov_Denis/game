//
// Created by mrstranger on 19.12.18.
//

#ifndef CLIENT_OUT_SINGLTONE_HPP
#define CLIENT_OUT_SINGLTONE_HPP

#include <SFML/Graphics/Texture.hpp>
#include <memory>

class C_Singletone {
 public:
    static C_Singletone& Instance();

    std::shared_ptr<sf::Texture>& getTexturePlayer();
    std::shared_ptr<sf::Texture>& getTextureZombie();
    std::shared_ptr<sf::Texture>& getTextureBullet();
 private:
    C_Singletone();

    std::shared_ptr<sf::Texture> refTexturePlayer;
    std::shared_ptr<sf::Texture> refTextureZombie;
    std::shared_ptr<sf::Texture> refTextureBullet;
};


#endif //CLIENT_OUT_SINGLTONE_HPP
