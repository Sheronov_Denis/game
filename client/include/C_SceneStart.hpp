//
// Created by mrstranger on 15.12.18.
//

#ifndef OTHER_C_SCENESTART_HPP
#define OTHER_C_SCENESTART_HPP

#include <imgui.h>
#include <imgui-SFML.h>
#include "C_SceneManager.hpp"
#include "C_SceneLoad.hpp"

class C_SceneStart : public C_Scene {
 public:
    C_SceneStart(C_SceneManager& _sceneManager, std::shared_ptr<C_SceneLoad> _loadScene, sf::TcpSocket& _socket);

    void OnCreate() override;
    void OnDestroy() override;

    void OnActivate() override;

    void HandleInput() override;
    void Update(float deltaTime) override;
    void Draw(sf::RenderWindow& window) override;

    bool CheckInput();

    void SetSwitchToScene(unsigned int id);
    void SetPrevSwitchToScene(unsigned int id);

    void setIp(char* ip);
    void setPort(char* _port);

 private:
    sf::Texture backgroundTexture;
    sf::Sprite backgroundSprite;

    ImGuiWindowFlags window_flags;
    char window_title[16];
    bool open;
    bool* p_open;

    char ipBuf[255];
    char portBuf[255];
    std::string ip;
    ushort port;

    C_SceneManager& sceneManager;
    std::shared_ptr<C_SceneLoad> loadScene;
    unsigned int nextSceneID;
    unsigned int prevSceneID;

    sf::TcpSocket& socket;
};

#endif //OTHER_C_SCENESTART_HPP
