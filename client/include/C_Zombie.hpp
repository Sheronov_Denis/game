//
// Created by mrstranger on 02.12.18.
//

#ifndef OTHER_C_ZOMBIE_HPP
#define OTHER_C_ZOMBIE_HPP

#include <string>
#include <SFML/Audio/Sound.hpp>
#include "C_Entity.hpp"

#define LIFETIME 5e3

class C_Zombie : public C_Entity {
 public:
    C_Zombie(const std::shared_ptr<sf::Texture>& _texture, int _id, float _x, float _y, int _w, int _h);

    void update(float time) override;
    void checkStates(const pack::ServObjInfo& mes);


    float lifeTime;
    bool isErase;

    sf::Sound hitSound;
};

#endif //OTHER_C_ZOMBIE_HPP
