//
// Created by mrstranger on 15.12.18.
//

#ifndef OTHER_C_SCENELOAD_HPP
#define OTHER_C_SCENELOAD_HPP

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Network/TcpSocket.hpp>
#include <imgui.h>
#include <imgui-SFML.h>
#include "C_Scene.hpp"
#include "C_SceneManager.hpp"
#include "C_SceneGame.hpp"

class C_SceneLoad : public C_Scene {
 public:
    C_SceneLoad(sf::RenderWindow& _window, C_SceneManager& sceneManager, std::shared_ptr<C_SceneGame> gameScene, sf::TcpSocket& _socket, sf::Packet& _packet);

    void OnCreate() override;
    void OnDestroy() override;

    void OnActivate() override;

    void HandleInput() override;
    void Update(float deltaTime) override;
    void LateUpdate(float deltaTime) override;
    void Draw(sf::RenderWindow& window) override;

    void SetSwitchToScene(unsigned int id);
    void SetPrevSwitchToScene(unsigned int id);

    void setIp(char* ip);
    void setPort(char* _port);

    void setIp(std::string& _ip);
    void setPort(ushort _port);

 private:
    sf::Texture backgroundTexture;
    sf::Sprite backgroundSprite;

    ImGuiWindowFlags window_flags;
    char window_title[16];
    bool open;
    bool* p_open;

    sf::TcpSocket& socket;
    sf::Packet& packet;
    sf::RenderWindow& window;

    std::string ip;
    ushort port;

    C_SceneManager& sceneManager;
    std::shared_ptr<C_SceneGame> gameScene;
    unsigned int nextSceneID;
    unsigned int prevSceneID;

    bool isFullSession;
};


#endif //OTHER_C_SCENELOAD_HPP
