#ifndef C_PLAYER_HPP
#define C_PLAYER_HPP

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <C_Entity.hpp>
#include <iostream>
#include "game.pb.h"

enum class Weapon {
    M16,
    Drob
};

enum class Masks {
    Rabbit,
    Tiger
};

class C_Player : public C_Entity {
 public:
    C_Player(const std::shared_ptr<sf::Texture>& _texture, int _id, float _x, float _y, int _w, int _h);

    void update(float time) override;
    void checkWeapon(const pack::ServObjInfo& mes);
    void checkStates(const pack::ServObjInfo& mes);

    Weapon weapon;
    uint32_t points;

    sf::Texture textureMask;
    Masks mask;

    sf::Sound curSound;
    bool isShooting;
};

#endif  // C_PLAYER_HPP
