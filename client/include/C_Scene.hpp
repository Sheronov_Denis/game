//
// Created by mrstranger on 15.12.18.
//

#ifndef OTHER_C_SCENE_HPP
#define OTHER_C_SCENE_HPP

#include <SFML/Graphics/RenderWindow.hpp>

class C_Scene {
public:
    virtual void OnCreate() = 0;
    virtual void OnDestroy() = 0;

    virtual void OnActivate() {};
    virtual void OnDeactivate() {};

    virtual void HandleInput() {};
    virtual void Update(float deltaTime) {};
    virtual void LateUpdate(float deltaTime) {};
    virtual void Draw(sf::RenderWindow& window) {};

    bool isOpen = true;
    bool IsOpen() {
        return isOpen;
    }
};

#endif //OTHER_C_SCENE_HPP
