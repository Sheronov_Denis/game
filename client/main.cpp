#include <SFML/Network.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <string>

#include "C_Main.hpp"
#include "C_MainManager.hpp"
#include "C_Singletone.hpp"
#include "C_SingleSound.hpp"

int main(int argc, const char* argv[]) {
    C_Singletone::Instance();
    C_SingleSound::Instance();

    C_MainManager world;
    while (world.IsRunning()) {
        C_Main game;
        while (game.IsRunning()) {
            game.CaptureInput();
            game.Update();
            game.LateUpdate();
            game.Draw();

            game.CalculateDeltaTime();
        }
        if (!game.IsRepeat()) {
            world.setIsRunning(false);
        }

        ImGui::SFML::Shutdown();
    }

    return 0;
}
